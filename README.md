# ![logo](resources/puolukkalogo-small.png) Puolukkakalenteri

![screenshot](resources/screenshot.png)
![screenshot](resources/screenshot2.png)

Avaa kalenteri ajamalla kalenteri.py

Asennus Linuxilla (luo desktop entryn ~/.local/share/applications kansioon ja suorittamisskriptin ~/.local/bin kansioon):
- mene kansioon jossa on install.sh
- suorita install.sh: `./install.sh`

[Asennus Linuxilla (lisää ohjeita)](resources/linux.md)

[Asennus Windowsilla](resources/windows.md)

[Dokumentaatio](resources/dokumentaatio.md)
