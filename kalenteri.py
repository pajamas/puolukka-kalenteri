#!/usr/bin/python3
# coding: utf-8
import tkinter
from tkinter import ttk
from calendar import monthrange
from datetime import date
from classes import AddReminderWin, Weekdays, Months, Holidays, Holiday, Month, Button
from reminders import read_reminders, todays_reminders, add_reminder, del_reminder
from loadpaths import puolukka_png, icon_png, arrow_png
from settings import settings
from info import info
from dateutil.easter import *
import tkinter.font as tkFont

class State:
    def __init__(self, theme=""):
        self.days = []
        if theme == "":
            self.window = tkinter.Tk()
        self.what_font()
        self.puolukka = tkinter.PhotoImage(file=puolukka_png())
        self.SCREEN_WIDTH = self.window.winfo_screenwidth()
        self.SCREEN_HEIGHT = self.window.winfo_screenheight()
        if theme=="light" or theme=="":
            self.light_colors()
        else:
            self.dark_colors()
        self.main_frame = tkinter.Frame(self.window,background = self.BG_COLOR)
        self.calendar_grid_frame = tkinter.Frame(self.main_frame)
        self.head_frame = tkinter.Frame(self.main_frame, padx=1,pady=4,
                background = self.BG_COLOR, 
                width=self.main_frame.winfo_width())
        self.icon_frame = tkinter.Frame(self.head_frame,bg=self.BG_COLOR, 
                                        height=20,width=23)
        self.icon_frame2 = tkinter.Frame(self.head_frame,bg=self.BG_COLOR,
                                         height=20,width=23)
        self.icon = tkinter.Label(self.icon_frame,image=self.puolukka,
                                  bg=self.BG_COLOR,padx=5)
        self.icon2 = tkinter.Label(self.icon_frame2,image=self.puolukka,
                                   bg=self.BG_COLOR,padx=5)
        self.labelFrame = tkinter.Frame(self.head_frame,bg=self.BG_COLOR,
                                        width=145,height=26)
        self.font_size = 15
        self.weekdays = Weekdays()
        self.months = Months()
        self.holidays = Holidays()
        self.month_viewing = Month(date.today().month,date.today().year)
        self.current_month = Month(date.today().month,date.today().year)
        self.down_arrow = tkinter.PhotoImage(file=arrow_png())
        self.footer = tkinter.Frame(self.main_frame, padx=1,pady=0,
                background = self.BG_COLOR)
        self.menu_button = Button(self.footer, "", self, bg = self.REMINDER_BG,
                                          bg_hover = self.REMINDER_HOVER_BG,
                                          image = self.down_arrow,
                                          height = 20, width = 20)
        self.menu_button.bind(self.expand)
        self.menu = tkinter.Frame(self.footer,bg=self.BG_COLOR)
        
        self.plus_button = Button(container = self.menu, text = "+", state = self,
                                  pad_x = 6, pad_y = 1)
        self.info_button = Button(self.menu, "i", self,
                                  pad_x = 6, pad_y = 1)
        self.settings_button = Button(self.menu, "asetukset", self,
                                  pad_x = 6, pad_y = 1)
        self.plus_button.bind(add_reminder, (date.today().day-1, self))
        self.info_button.bind(info, (self,))
        self.settings_button.bind(settings, (self,))
        self.rows = []
        self.expanded = False
        self.footer_label = tkinter.Label(self.footer,text=f"{self.weekdays.long[date.today().weekday()]} {date.today().strftime('%d.%m.%Y')}", 
                    bg=self.BG_COLOR, fg=self.FG_PURPLE,           
                    font=(self.font,self.font_size-3))
        self.footer_rows_frame = tkinter.Frame(self.footer,bg=self.LIGHT_PINK)
        self.footer_rows_frame.columnconfigure(0,weight=1)
        self.head_label = tkinter.Label(self.labelFrame,
                text = str(self.month_viewing), 
                font=(self.font, self.font_size-1), background = self.BG_COLOR,
                foreground = self.months.colors[self.month_viewing.month-1],
                )
        self.reminders = read_reminders(self)
        self.repeating_reminders = []

    def start(self):
        self.make_main_win()
        self.gen_month()
        self.window.deiconify()
        self.window.mainloop()

    def what_font(self):
        fonts = [("Noto Serif",15),("Times New Roman",16),
                ("Palatino Linotype",16),("Palatino",16)]
        for font in fonts:
            if font[0] in list(tkFont.families()):
                self.font = font[0]
                self.font_size = font[1]
                break

    def switch_theme(self):
        if self.theme=="light":
            self.remake("dark")
        else:
            self.remake("light")

    def light_colors(self):
        self.theme = "light"
        #self.BG_COLOR = "#ffbbcb"
        self.BG_COLOR = "#FFADC9"
        #self.DAY_GRID_BG = "#FFADC1"
        self.DAY_GRID_BG = "#FFBDD7"
        #self.REMINDER_BG = "#f099aa"
        self.REMINDER_FG = "#ffffff"
        self.REMINDER_HOVER_FG = "#ffffff"
        self.REMINDER_BG = "#FFA3C2"
        #self.REMINDER_HOVER_BG = "#ffa9bc"
        self.REMINDER_HOVER_BG = "#FFC2DD"
        #self.TODAY_FG = "#8f002c"
        self.TODAY_FG = "#B2005F"
        self.FG_BLUE = "#4838b1"
        self.FG_PURPLE = "#823478"
        self.FG_PURPLE_2 = "#A8336A"
        self.FG_PURPLE_3 = "#C5407A"
        self.FG_PURPLE_HOVER = "#c54060"
        #self.LIGHT_PINK = "#ffcbdb"
        self.LIGHT_PINK = "#FFCBDD"
        self.LIGHTER_PINK = "#FFCFE2"
        self.LIGHTEST_PINK = "#ffdcea"
        self.LIGHTEST_PINK_2 = "#FFEBF3"
        self.DAY_BG = self.LIGHTEST_PINK
        self.BUTTON_BG = "#F4EDB4"
        self.BUTTON_FG = "#AB3574"
        self.HOVER_BUTTON_BG = "#F9F5D7"
        self.LIGHT_BLUE = "#D7DBF9"
        self.HOVER_BUTTON_FG = "#a32099"
        self.RED = "#FD4E70"
        self.FOOTER_ROWS_BG = self.LIGHTEST_PINK

    def dark_colors(self):
        self.theme = "dark"
        self.BG_COLOR = "#111111"
        self.DAY_GRID_BG = "#151515"
        self.REMINDER_BG = "#3C1620"
        self.REMINDER_HOVER_BG = "#4B1B28"
        self.TODAY_FG = "#af305c"
        self.FG_BLUE = "#4838b1"
        self.FG_PURPLE = "#b244a8"
        self.FG_PURPLE_HOVER = "#c54060"
        self.LIGHT_PINK = "#222222"
        self.LIGHTER_PINK = "#242424"
        self.LIGHTEST_PINK = "#262626"
        self.LIGHTEST_PINK_2 = "#404040"
        self.DAY_BG = self.LIGHTEST_PINK
        self.BUTTON_BG = "#222222"
        self.BUTTON_FG = "#af3164"
        self.HOVER_BUTTON_BG = "#282828"
        self.HOVER_BUTTON_FG = "#C02691"
        self.RED = "#FD4E70"
        self.FOOTER_ROWS_BG = self.DAY_GRID_BG

    def remake(self,theme):
        self.shrink()
        self.footer.destroy()
        self.__init__(theme)
        self.make_main_win()
        self.start()

    def isholiday(self,day,month=0):
        if month == 0:
            month = self.month_viewing.month
        for holiday in self.holidays.list:
            if holiday.date == (day,month):
                return holiday
        return False

    def prev_month(self):
        self.month_viewing -= 1
        self.update_month()

    def next_month(self):
        self.month_viewing += 1
        self.update_month()

    def make_main_win(self):
        # window config and binds
        self.window.title("Kalenteri")
        self.window.configure(bg = self.BG_COLOR)
        self.window.resizable(False, False)

        self.main_frame.grid(row = 0, column = 0, sticky = "nsew", pady = (1,1))
    
        self.window.bind('<Escape>', lambda e : state.window.destroy())
        
        self.window.bind('<Left>', lambda e : state.prev_month())
        self.window.bind('<Right>', lambda e : state.next_month())

        self.window.bind('<Up>', lambda e : state.shrink())
        self.window.bind('<Down>', lambda e : state.expand())

        self.window.bind('i', lambda e : info(self))
        self.window.bind('a', lambda e : add_reminder(date.today().day-1,self))

        self.window.bind('u', lambda e : self.update_month())

        self.window.bind('d', lambda e : self.switch_theme())
    
    def update_month(self):
        self.head_label['text'] = str(self.month_viewing)
        self.head_label['foreground'] = self.months.colors[self.month_viewing.month-1]

        self.calendar_grid_frame.destroy()

        # make calendar grid
        daysnum = monthrange(*reversed(state.month_viewing))[1]
        self.calendar_grid_frame = tkinter.Frame(self.main_frame, background=self.DAY_GRID_BG)
        self.calendar_grid_frame.grid(row=1, sticky="nsew")
        self.days = [Button(self.calendar_grid_frame, state=self, 
                            font_size=self.font_size-4,
                            height=1, width=1, pad_x=11.5, pad_y=5.5,
                            margin_x = 1, margin_y = 1,
                            bg=self.DAY_BG, fg=self.FG_PURPLE_2,
                            text = str(i+1),
                            bg_hover=self.LIGHTEST_PINK_2, 
                            fg_hover=self.FG_PURPLE_HOVER) for i in range(daysnum)]
        
        firstday = date(*reversed(self.month_viewing), 1)
        colstart = firstday.weekday()
        
        self.repeating_reminders = []
        # calculate repeating reminders for this month
        for reminder in self.reminders:
            repeats = []
            if reminder.repeating:
                repeats = reminder.get_repeats_in_month(self.month_viewing)
            for reminder in repeats:
                self.repeating_reminders.append(reminder)

        for reminder in self.repeating_reminders:
            day = reminder.date.day-1
            self.days[day].set_bg(self.REMINDER_BG)
            self.days[day].set_hover_bg(self.REMINDER_HOVER_BG)
            self.days[day].set_fg(self.REMINDER_FG)
            self.days[day].set_hover_fg(self.REMINDER_HOVER_FG)

        # change color of days with reminders
        for reminder in self.reminders:
            if state.month_viewing.includes_date(reminder.date):
                day = reminder.date.day-1
                self.days[day].set_bg(self.REMINDER_BG)
                self.days[day].set_hover_bg(self.REMINDER_HOVER_BG)
                self.days[day].set_fg(self.REMINDER_FG)
                self.days[day].set_hover_fg(self.REMINDER_HOVER_FG)
        
        # put days into calendar grid
        for i in range(daysnum):
            self.days[i].grid(row=(int((i+colstart)/7)+1),column=(i+colstart)%7)
            self.days[i].bind(add_reminder, (i, state))

            # holiday colors
            for holiday in self.holidays.list:
                holiday.get_date(self.month_viewing.year)
                if self.month_viewing.month == holiday.date[1] and i+1 == holiday.date[0]:
                    self.days[i].set_bg(holiday.color)
                    self.days[i].set_fg("#ffffff")
                    self.days[i].no_hover_color()
        rownum = int((daysnum-1+colstart)/7)+1

        # make current day look different in grid
        if (date.today().month,date.today().year) == state.month_viewing():
                self.days[date.today().day-1].set_fg(self.TODAY_FG)
                self.days[date.today().day-1].label['borderwidth'] = 1
                self.days[date.today().day-1].label['padx'] -= 1
                self.days[date.today().day-1].label['pady'] -= 1
                self.days[date.today().day-1].label['relief'] = "raised"

    def gen_month(self):
            # create frame for header
            self.head_frame.grid(row=0,sticky="we")
            
            # create buttons for navigating months
            month_buttons = [Button(self.head_frame, "⮜", self, 
                                             font_size = 10, pad_y = 2, pad_x = 5),
                                      Button(self.head_frame, "⮞" ,self, 
                                             font_size = 10, pad_y = 2, pad_x = 5)]
            month_buttons[0].bind(self.prev_month)
            month_buttons[1].bind(self.next_month)
            month_buttons[0].grid(r=0,c=0,sticky="w")
            month_buttons[1].grid(r=0,c=10,sticky="e")


            # make label that says name of month + year
            self.labelFrame.grid(row=0,column=2)
            self.head_label.place(relx=.5, rely=.5, anchor=tkinter.CENTER)
            
            # make icons
            self.icon_frame.grid(row=0,column=9,sticky="e")
            self.icon_frame2.grid(row=0,column=1,sticky="w")
            self.icon.place(relx=.5, rely=.5, anchor=tkinter.CENTER)
            self.icon2.place(relx=.5, rely=.5, anchor=tkinter.CENTER)
            
            self.update_month()

            #separator = ttk.Separator(self.main_frame, orient='horizontal')
            #separator.grid(row=2,column=0,sticky="we")

            self.footer.grid(row=3,sticky="nsew")
            self.footer.columnconfigure(0,weight=1)
            
            self.footer_label.grid(row=0,column=0,sticky="nsew")
            self.menu_button.grid(r=0,c=1,sticky="e")
        
            if self.expanded:
                self.footer_refresh()
    
    def expand(self):
        if self.expanded:
            return
        self.expanded = True
        self.menu_button.bind(self.shrink)

        state.footer_refresh()

    def footer_refresh(self):
        self.footer_rows_frame.grid_forget()
        
        for child in self.footer_rows_frame.winfo_children():
            child.grid_forget()
    
        self.rows = []
        # list today's reminders
        for reminder in self.reminders:
            if reminder.date == date.today():
                row_label = tkinter.Label(self.footer_rows_frame,
                    text = f"{reminder.text}",
                    bg = state.FOOTER_ROWS_BG, fg = self.FG_PURPLE, 
                    justify = "left", wraplength = 200)
                row_delete_button = Button(self.footer_rows_frame,
                    text = "⨯", state = self, pad_x = 3, pad_y = 0,
                    bg = state.LIGHT_PINK, fg = self.FG_PURPLE)
                row_delete_button.bind(del_reminder, (reminder.ID, self))
                self.rows.append((row_label, row_delete_button, reminder.ID))

        # list holidays
        for holiday in self.holidays.list:
            if holiday.date == (date.today().day,date.today().month):
                self.rows.append((tkinter.Label(self.footer_rows_frame,text=f"{holiday.name}",
                    bg=holiday.color,fg="#ffffff",justify="left",wraplength=200), None, None))

        # put menu in grid
        self.menu.grid(column=0,row=50,sticky="nsew")
        self.plus_button.grid(c=0,r=0)
        self.info_button.grid(r=0,c=1)
        self.settings_button.grid(c=2,r=0)

        # put reminders in grid
        if len(self.rows)>0:
            self.footer_rows_frame.grid(column=0,row=1,sticky="nsew",columnspan=2)
            for i,row in enumerate(self.rows):
                row[0].grid(row=i,column=0,sticky="nsew")
                if row[1]:
                    row[1].grid(r = i, c = 1, sticky = "e")
        elif self.month_viewing == self.current_month:
            self.days[date.today().day-1].set_bg(self.DAY_BG)
            self.days[date.today().day-1].set_hover_bg(self.LIGHTEST_PINK_2)

    def shrink(self):
        if not self.expanded:
            return
        self.expanded = False
        self.menu_button.bind(self.expand)
        
        self.footer_rows_frame.grid_forget()

        self.menu.grid_forget()
        self.plus_button.label.grid_forget()
        self.info_button.label.grid_forget()
        self.settings_button.label.grid_forget()

state = State()
state.window.iconphoto(True, tkinter.PhotoImage(file=icon_png()))
state.window.withdraw()
state.what_font()

todays_reminders(state)
