import tkinter
from tkinter import ttk
from datetime import date,datetime
from sys import stderr
from loadpaths import reminders_txt
from classes import AddReminderWin, Reminder, Button, IntervalType
from util import find

def write_reminder(state, add_reminder_win):
    # get text for reminder, return if empty
    reminderText = add_reminder_win.reminder_entry.get()

    if reminderText == "":
        return

    repeat = add_reminder_win.repeating
    if repeat:
        repeating_interval = int(add_reminder_win.numbox.get())
        types = ["","päivä","viikko","kuukausi","vuosi"]
        repeating_type = IntervalType(types.index(add_reminder_win.typebox.get()))
        repeat_forever = int(add_reminder_win.repeat_forever)
        repeat_times = 1
        if not repeat_forever:
            repeat_times = int(add_reminder_win.numbox_2.get())

    reminder = None
    # add reminder to state.reminders list
    if repeat:
        reminder = Reminder(date(*reversed(state.month_viewing),
            add_reminder_win.day+1), reminderText, repeating = True, 
            interval = repeating_interval, interval_type = repeating_type, 
            repeat_forever = repeat_forever, repeat_times = repeat_times)
        reminder.make_id(state.id_list)
        state.reminders.append(reminder)
    else:
        reminder = Reminder(date(*reversed(state.month_viewing),add_reminder_win.day+1),reminderText)
        reminder.make_id(state.id_list)
        state.reminders.append(reminder)

    # write reminder to file
    if repeat:
        with open(reminders_txt(),"a",encoding="utf-8") as f:
            f.write(f"repeat,{reminder.ID},{reminder.date.day},{repr(state.month_viewing)},{repeating_interval},{repeating_type.value},{repeat_forever},{repeat_times},{reminderText}\n")
    else:
        with open(reminders_txt(),"a",encoding="utf-8") as f:
            f.write(f"norepeat,{reminder.ID},{add_reminder_win.day+1},{repr(state.month_viewing)},{reminderText}\n")

    # change day's bgcolor in grid
    if not state.isholiday(add_reminder_win.day+1,state.month_viewing.month):
        state.days[add_reminder_win.day].set_bg(state.REMINDER_BG)
        state.days[add_reminder_win.day].set_hover_bg(state.REMINDER_HOVER_BG)
    
    if repeat:
        state.update_month()
    
    # close the add reminder window
    add_reminder_win.close()
 
    # if main window expanded and adding a reminder for today,
    # add row to footer reminder list
    if state.expanded and date.today() == date(*reversed(state.month_viewing),add_reminder_win.day+1):
        state.footer_refresh()

def del_reminder(ID, state, add_reminder_win = None):
    # destroy reminder
    if add_reminder_win: # delete from AddReminderWin window
        i = find(add_reminder_win.rows, lambda row: row[2] == ID)

        if i != 1:
            add_reminder_win.rows[i][0].destroy()
            add_reminder_win.rows[i][1].destroy()

            del add_reminder_win.rows[i]

            if len(add_reminder_win.reminder_list_frame.winfo_children()) == 0:
                add_reminder_win.reminder_list_frame.grid_forget()
                state.days[add_reminder_win.day].set_bg(state.LIGHTEST_PINK)
                state.days[add_reminder_win.day].set_hover_bg(state.LIGHTEST_PINK_2)

    # delete from main window
    if len(state.rows):
        i = find(state.rows, lambda row: row[2] == ID)
        if i != -1:
            state.rows[i][0].destroy()
            state.rows[i][1].destroy()
            del state.rows[i]

    # read lines from reminders file and write all but deleted one back into it
    with open(reminders_txt(),"r",encoding="utf-8") as fp:
        lines = fp.readlines()
    with open(reminders_txt(),"w",encoding="utf-8") as f:
        for line in lines:
            if int(line.split(",")[1]) != ID:
                f.write(line)

    # remove from state.reminders list
    i = find(state.reminders, lambda r: r.ID == ID)
    if i != -1:
        del state.reminders[i]

    state.update_month()
    
    if state.expanded:
        state.footer_refresh()
    else:
        add_reminder_win_refresh(add_reminder_win, state, add_reminder_win.day)


def show_recurring_options(add_reminder_win):
    add_reminder_win.options_frame.grid(row=3,column=0,sticky="nsew")
    add_reminder_win.checkbox.label['text'] = "☒ toistuva muistutus"
    add_reminder_win.checkbox.command = hide_recurring_options
    add_reminder_win.repeating = True


def hide_recurring_options(add_reminder_win):
    add_reminder_win.options_frame.grid_forget()
    add_reminder_win.checkbox.label['text'] = "☐ toistuva muistutus"
    add_reminder_win.checkbox.command = show_recurring_options
    add_reminder_win.repeating = False


def show_recurring_options_2(add_reminder_win):
    add_reminder_win.options_frame_2.grid(row=2,column=0,sticky="nsew", columnspan=4)
    add_reminder_win.checkbox_2.label['text'] = "☐ ikuisesti"
    add_reminder_win.checkbox_2.command = hide_recurring_options_2
    add_reminder_win.repeat_forever = 0


def hide_recurring_options_2(add_reminder_win):
    add_reminder_win.options_frame_2.grid_forget()
    add_reminder_win.checkbox_2.label['text'] = "☒ ikuisesti"
    add_reminder_win.checkbox_2.command = show_recurring_options_2
    add_reminder_win.repeat_forever = 1


def add_reminder(day: int, state):
    # create instance of AddReminderWin
    add_reminder_win = AddReminderWin(state.BG_COLOR)

    add_reminder_win.day = day

    # create window and main frame
    add_reminder_win.window = tkinter.Toplevel()
    add_reminder_win.window.title("Lisää muistutus")
    add_reminder_win.window.configure(bg=add_reminder_win.bg_color) 
    add_reminder_win.window.resizable(False, False)
    add_reminder_win.window.bind('<Escape>', lambda e : add_reminder_win.close())

    add_reminder_win.main_frame = tkinter.Frame(add_reminder_win.window,
            background=add_reminder_win.bg_color,
            padx=3,pady=3)
    add_reminder_win.main_frame.grid(row=0,sticky="nsew")
    add_reminder_win.main_frame.grid_columnconfigure(0, weight=1)

    # create window header
    label = tkinter.Label(add_reminder_win.main_frame,
            text=f"Lisää muistutus: {state.weekdays.short[datetime(*reversed(state.month_viewing),day+1).weekday()]} {day+1}.{state.month_viewing.month}.{state.month_viewing.year}",
            background=add_reminder_win.bg_color,foreground=state.FG_BLUE,font=(state.font,state.font_size-2))

    label.grid(row=0, columnspan = 5,sticky="w",padx=3)

    # create entry for new reminder
    add_reminder_win.reminder_entry = tkinter.Entry(add_reminder_win.main_frame,
            bg=state.LIGHTER_PINK,bd=1,fg=state.FG_PURPLE,font=("",state.font_size-4))
    add_reminder_win.reminder_entry.grid(row=1,column=0,padx=(0,4),sticky="we")
    
    add_reminder_win.reminder_entry.focus()

    # checkbox
    add_reminder_win.checkbox = Button(add_reminder_win.main_frame, "☐ toistuva muistutus", 
               state,
               bg=add_reminder_win.bg_color, bg_hover=add_reminder_win.bg_color,
               command=show_recurring_options, args=(add_reminder_win,))
    add_reminder_win.checkbox.grid(r=2,c=0,sticky="w")

    # options (hidden)
    add_reminder_win.options_frame = tkinter.Frame(add_reminder_win.main_frame,
                                                   bg=add_reminder_win.bg_color,
                                                   padx=3,pady=3)
    text1 = tkinter.Label(add_reminder_win.options_frame, bg=add_reminder_win.bg_color,
                          text="muistuta joka")
    add_reminder_win.numbox = ttk.Spinbox(add_reminder_win.options_frame,
                                          from_ = 1, to = 31, width = 3)
    add_reminder_win.numbox.set(1)
    add_reminder_win.typebox = ttk.Combobox(add_reminder_win.options_frame, justify="left",
                                            values=["päivä","viikko","kuukausi","vuosi"],
                                            width = 8)
    add_reminder_win.typebox.set("viikko")
    add_reminder_win.checkbox_2 = Button(add_reminder_win.options_frame, "☒ ikuisesti", state,
               bg=add_reminder_win.bg_color, bg_hover=add_reminder_win.bg_color,
               command=show_recurring_options_2, args=(add_reminder_win,))

    text1.grid(row=0,column=0)
    add_reminder_win.numbox.grid(row=0,column=1)
    add_reminder_win.typebox.grid(row=0,column=2)
    add_reminder_win.checkbox_2.grid(row=1,column=0) 

    add_reminder_win.options_frame_2 = tkinter.Frame(add_reminder_win.options_frame,
                                                   bg=add_reminder_win.bg_color,
                                                   padx=3,pady=3)
    add_reminder_win.numbox_2 = ttk.Spinbox(add_reminder_win.options_frame_2,
                                          from_ = 1, to = 1000, width = 4)
    add_reminder_win.numbox_2.set(2)
    text2 = tkinter.Label(add_reminder_win.options_frame_2, bg=add_reminder_win.bg_color,
                          text="muistuta")
    text3 = tkinter.Label(add_reminder_win.options_frame_2, bg=add_reminder_win.bg_color,
                          text="kertaa")
    text2.grid(row=0,column=0)
    add_reminder_win.numbox_2.grid(row=0,column=1)
    text3.grid(row=0,column=2)
    

    # create add button
    add_reminder_button = Button(add_reminder_win.main_frame, "+", state,
                                 pad_x = 8, pad_y = 0, height = 0,
                                 font_size = state.font_size-1)
    add_reminder_button.bind(write_reminder, (state, add_reminder_win))
    add_reminder_button.grid(r = 1, c = 1, sticky = "e")
    
    # bind enter key to entry (press enter to add reminder)
    add_reminder_win.reminder_entry.bind('<Return>',
            lambda e : write_reminder(state,add_reminder_win))

    # list day's reminders under entry
    add_reminder_win.reminder_list_frame = tkinter.Frame(add_reminder_win.main_frame,
                                                         bg=state.LIGHT_PINK)
    add_reminder_win.reminder_list_frame.grid(row=10,sticky="nsew",columnspan="2")
    add_reminder_win.reminder_list_frame.grid_columnconfigure(0, weight=1)
    
    add_reminder_win_refresh(add_reminder_win, state, day)


def add_reminder_win_refresh(add_reminder_win, state, day):
    for child in add_reminder_win.reminder_list_frame.winfo_children():  
        child.grid_forget()
        
    add_reminder_win.rows = []
    
    for reminder in state.reminders:
        if reminder.date == date(*reversed(state.month_viewing),day+1):
            row_label = tkinter.Label(add_reminder_win.reminder_list_frame,
                text=f"{reminder.text}",
                bg=state.FOOTER_ROWS_BG,fg=state.FG_PURPLE,justify="left",
                wraplength=220)
            row_delete_button = Button(add_reminder_win.reminder_list_frame,
                "⨯", state, pad_x=5, pad_y=0,
                bg=state.LIGHT_PINK, fg=state.FG_PURPLE)
            add_reminder_win.rows.append((row_label, row_delete_button, reminder.ID))

    for reminder in state.repeating_reminders:
        if reminder.date == date(*reversed(state.month_viewing),day+1) and not reminder.original_date == reminder.date:
            row_label = tkinter.Label(add_reminder_win.reminder_list_frame,
                text=f"{reminder.text}",
                bg=state.FOOTER_ROWS_BG,fg=state.FG_PURPLE,justify="left",
                wraplength=220)
            row_delete_button = Button(add_reminder_win.reminder_list_frame,
                "⨯", state, pad_x=5, pad_y=0,
                bg=state.LIGHT_PINK, fg=state.FG_PURPLE)
            add_reminder_win.rows.append((row_label, row_delete_button, reminder.ID))

    for holiday in state.holidays.list:
        if holiday.date == (day+1,state.month_viewing.month):
            add_reminder_win.rows.append((tkinter.Label(add_reminder_win.reminder_list_frame,
                text=f"{holiday.name}",
                bg=holiday.color,fg="#ffffff",justify="left", wraplength=220), None, None))

    # put reminders in grid
    for i,row in enumerate(add_reminder_win.rows):
        if row[1]:
            row[0].grid(row=i,column=0,sticky="nsew")
            row[1].grid(r = i, c = 1, sticky = "e")
            row[1].bind(del_reminder, (row[2], state, add_reminder_win))
        else:
            row[0].grid(row=i,column=0,sticky="nsew",columnspan=2)

def close_todays_reminders(state,today_reminder_win):
    today_reminder_win.destroy()
    state.start()

def todays_reminders(state):
    todayStuff = 0
    for reminder in state.reminders:
        if reminder.date == date.today():
            todayStuff += 1
    for reminder in state.repeating_reminders:
        if reminder.date == date.today() and not reminder.original_date == reminder.date:
            todayStuff += 1
    # if there are no reminders, start main window
    if todayStuff == 0 and not state.isholiday(date.today().day):
        state.start()
        return
    
    # create window
    today_reminder_win = tkinter.Toplevel(state.window)
    today_reminder_win.resizable(False, False)

    main_frame = tkinter.Frame(today_reminder_win,bg=state.BG_COLOR,pady=5,padx=7)
    main_frame.grid(row=0,sticky="nsew")

    rows = []
    for reminder in state.reminders: # put todays reminders into labels in rows list
        if reminder.date == date.today():
            rows.append(tkinter.Label(main_frame,text=f"{reminder.text}",
                bg=state.LIGHT_PINK, fg=state.FG_PURPLE, wraplength=250))
    for reminder in state.repeating_reminders:
        if reminder.date == date.today() and not reminder.original_date == reminder.date:
            rows.append(tkinter.Label(main_frame,
                text=f"{reminder.text}",
                bg=state.LIGHT_PINK,fg=state.FG_PURPLE,
                wraplength=250))

    # if holiday, add it to rows
    holiday = state.isholiday(date.today().day)
    if holiday:
        rows.append(tkinter.Label(main_frame,text=f"{holiday.name}",
            bg=holiday.color,fg="#ffffff", wraplength=250))

    # config window and header
    today_reminder_win.title("Muistutukset")
    today_reminder_win.configure(bg=state.BG_COLOR)
    tod_win_title = tkinter.Label(main_frame,text="Tänään luvassa:",fg=state.FG_BLUE,bg=state.BG_COLOR,font=(state.font,14))
    tod_win_title.grid(row=0,padx=3,pady=2)

    separator = ttk.Separator(main_frame, orient='horizontal')
    separator.grid(row=1,column=0,sticky="we")

    for i,row in enumerate(rows): # put rows in grid
        row.grid(row=i+2,sticky="we")

    separator2 = ttk.Separator(main_frame, orient='horizontal')
    separator2.grid(row=99,column=0,sticky="we")

    # create ok button
    ok_button = Button(main_frame, "OK", state, pad_y = 4, pad_x = 8, margin_y = (5,0))
    ok_button.bind(close_todays_reminders, (state,today_reminder_win))
    ok_button.grid(row=100)

    # binds
    today_reminder_win.bind('<Escape>', lambda e : close_todays_reminders(state,today_reminder_win))
    today_reminder_win.bind('<Return>', lambda e : close_todays_reminders(state,today_reminder_win))

    today_reminder_win.focus()
    today_reminder_win.mainloop()


def read_reminders(state):
    reminders = []
    try:
        with open(reminders_txt(),"r",encoding="utf-8") as f: 
            lines = f.readlines()
            try:
                lines.remove('\n')
            except ValueError:
                pass
            idless = []
            id_list = []
            reminder = None
            for line in lines:
                if line.split(",")[0] == "repeat":
                    reminder = line.rstrip().split(",", maxsplit=9)
                    reminder = Reminder( date(int(reminder[4]), int(reminder[3]),
                                int(reminder[2])), text = reminder[9],
                                repeating = True, interval = int(reminder[5]), 
                                interval_type = IntervalType(int(reminder[6])),
                                repeat_forever = int(reminder[7]),
                                repeat_times = int(reminder[8]), ID=int(reminder[1]))
                    id_list.append(reminder.ID)
                    reminders.append(reminder)
                elif line.split(",")[0] == "norepeat":
                    reminder = line.rstrip().split(",", maxsplit=5)
                    reminder = Reminder( date(int(reminder[4]),int(reminder[3]),
                                int(reminder[2])), text = reminder[5], 
                                ID=int(reminder[1]))
                    id_list.append(reminder.ID)
                    reminders.append(reminder)
                else: 
                    reminder = line.rstrip().split(",", maxsplit=3)
                    reminder = Reminder( date(int(reminder[2]),int(reminder[1]),
                                int(reminder[0])), text = reminder[3])
                    idless.append(reminder)

            for reminder in idless:
                reminder.make_id(id_list)
                id_list.append(reminder.ID)
                reminders.append(reminder)
            state.id_list = id_list

        if len(idless):
            with open(reminders_txt(), 'w', encoding="utf-8") as f:
                for reminder in reminders:
                    if reminder.repeating:
                        f.write(f"repeat,{reminder.ID},{reminder.date.day},{reminder.date.month},{reminder.date.year},{repeating_interval},{repeating_type.value},{repeat_forever},{repeat_times},{reminderText}\n")
                    else:
                        f.write(f"norepeat,{reminder.ID},{reminder.date.day},{reminder.date.month},{reminder.date.year},{reminderText}\n")


            
    except FileNotFoundError:
        stderr.write("No reminders loaded: file Reminders.txt not found\n")
        pass
    return reminders
