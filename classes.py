import tkinter
from calendar import monthrange
from datetime import date, timedelta
from dataclasses import dataclass
import enum

class AddReminderWin:
    def __init__(self, bg_color="#ffcbdb"):
        self.window = None
        self.main_frame = None
        self.reminder_list_frame = None
        self.reminder_entry = None
        self.rows = []
        self.day = 0
        self.bg_color = bg_color
        self.checkbox = None
        self.options_frame = None
        self.options_frame_2 = None
        self.numbox = None
        self.repeating = False
        self.typebox = None
        self.checkbox_2 = None
        self.repeat_forever = 0
    def close(self):
        self.window.destroy()
        del self

class Weekdays:
    def __init__(self):
        self.long = ["Maanantai","Tiistai","Keskiviikko","Torstai","Perjantai","Lauantai","Sunnuntai"]
        self.short = ["ma","ti","ke","to","pe","la","su"]

class Months:
    def __init__(self):
        self.names = ["Tammikuu","Helmikuu","Maaliskuu","Huhtikuu","Toukokuu","Kesäkuu","Heinäkuu","Elokuu","Syyskuu","Lokakuu","Marraskuu","Joulukuu"]
        #self.colors = ["#2c46af","#0a6f99","#008a78","#0f8038","#427628","#60740a","#905804","#b53812","#a60c38","#a60c76","#8a0b98","#690db9"]
        self.colors = ["#364CA5","#136B90","#126D61","#17783A","#446F2F","#6E6711","#93551B","#b53812","#a60c38","#a60c76","#8a0b98","#690db9"]

@dataclass
class Holiday:
    name: str
    color: str
    date: tuple = (0,0)
    def __str__(self) -> str:
        return self.name
    # https://code.activestate.com/recipes/576517-calculate-easter-western-given-a-year/
    def calc_easter(self, year):
        "Returns Easter as a date object."
        a = year % 19
        b = year // 100
        c = year % 100
        d = (19 * a + b - b // 4 - ((b - (b + 8) // 25 + 1) // 3) + 15) % 30
        e = (32 + 2 * (b % 4) + 2 * (c // 4) - d - (c % 4)) % 7
        f = d + e - 7 * ((a + 11 * d + 22 * e) // 451) + 114
        month = f // 31
        day = f % 31 + 1    
        return date(year, month, day)
    def get_date(self,year):
        date = 0
        if self.name=="Pääsiäinen":
            date = self.calc_easter(year)
        if self.name=="Juhannus":
            date = self.midsummer_date(year)
        if date: self.date = (date.day,date.month)
        return self.date
    def midsummer_date(self,year):
        for day in range(20,27):
            if date.weekday(date(year,6,day)) == 5:
                return date(year,6,day)

class Holidays:
    def __init__(self):
        self.jouluaatto = Holiday("Jouluaatto","#d3314f",(24,12))
        #self.itsenaisyyspaiva = Holiday("Itsenäisyyspäivä","#7093db",(6,12))
        self.itsenaisyyspaiva = Holiday("Itsenäisyyspäivä","#7C9BDA",(6,12))
        self.ystavanpaiva = Holiday("Ystävänpäivä","#eb75ab",(14,2))
        self.uudenvuodenaatto = Holiday("Uudenvuodenaatto","#b272b6",(31,12))
        #self.paasiainen = Holiday("Pääsiäinen","#F3D17C")
        self.paasiainen = Holiday("Pääsiäinen","#F7D57D")
        self.juhannus = Holiday("Juhannus","#D45E7B")
        self.list = [self.jouluaatto, self.itsenaisyyspaiva, self.ystavanpaiva, 
                self.uudenvuodenaatto, self.paasiainen, self.juhannus]

class Month:
    def __init__(self, month: int, year: int):
        self._month_names = ["Tammikuu","Helmikuu","Maaliskuu","Huhtikuu",
                "Toukokuu","Kesäkuu","Heinäkuu","Elokuu",
                "Syyskuu","Lokakuu","Marraskuu","Joulukuu"]
        self.month = month
        self.year = year
    def __str__(self) -> str:
        return f"{self._month_names[self.month-1]} {self.year}"
    def __repr__(self) -> str:
        return f"{self.month},{self.year}"
    def __reversed__(self) -> tuple:
        return (self.year,self.month)
    def __add__(self, n: int):
        self.year += (self.month + n - 1) // 12
        self.month = (self.month + n) % 12
        if self.month == 0:
            self.month = 12
        return self
    def __sub__(self, n: int):
        self.year += (self.month - n - 1) // 12
        self.month = (self.month - n) % 12
        if self.month == 0:
            self.month = 12
        return self
    def __call__(self) -> tuple:
        return (self.month,self.year)
    def includes_date(self, date: date) -> bool:
        if date.month == self.month and date.year == self.year:
            return True
        return False

class IntervalType(enum.Enum):
    NoRepeat = 0
    Days = 1
    Weeks = 2
    Months = 3
    Years = 4

@dataclass
class Reminder:
    date: date
    text: str
    repeating: bool = False
    interval: int = 0
    interval_type: IntervalType = IntervalType.NoRepeat
    repeat_forever: int = 0
    repeat_times: int = 1
    original: bool = True
    original_date: date = date
    ID: int = None
    def make_id(self, id_list):
        ID = 0
        while True:
            if ID in id_list:
                ID += 1
            else:
                self.ID = ID
                break
        id_list.append(ID)
    def __str__(self) -> str:
        return f"Reminder({str(self.date)},{self.text})"
    def __eq__(self, other):
        if self.ID == other.ID:
            return True
        return False
    def get_repeats_in_month(self, month: Month) -> list:
        repeats = []
        date1 = self.date
        lastday = monthrange(*reversed(month))[1]
        lastdate = date(*reversed(month), lastday)
        td = timedelta(1)
        original_date = self.date
        repetition_num = 0
        if self.repeat_forever:
            self.repeat_times = 1000000000
        
        reminder_year = self.date.year
        reminder_month = Month(self.date.month, self.date.year)
        if self.interval_type == IntervalType.Years:
            while reminder_year <= month.year and repetition_num < self.repeat_times:
                if reminder_year == month.year:
                    if month.includes_date(date(reminder_year, self.date.month,
                                                self.date.day)):
                        repeats.append(Reminder(date(reminder_year, self.date.month,
                                                self.date.day),
                                                self.text,
                                                original=False,
                                                original_date = original_date,
                                                ID = self.ID))
                reminder_year += self.interval
                repetition_num += 1
            return repeats
        
        elif self.interval_type == IntervalType.Months:
            while remider_month <= month and repetition_num < self.repeat_times:
                if reminder_month == month:
                    if self.date.day <= lastday:
                        repeats.append(Reminder(date(*reversed(month), self.date.day),
                                                self.text, original=False,
                                                original_date = original_date,
                                                ID = self.ID))
                    else:
                        repeats.append(Reminder(date(*reversed(month), lastday), 
                                                self.text, original=False,
                                                original_date = original_date,
                                                ID = self.ID))
                reminder_month += self.interval
                repetition_num += 1
            return repeats

        elif self.interval_type == IntervalType.Days:
            td = timedelta(days=self.interval)
        elif self.interval_type == IntervalType.Weeks:
            td = timedelta(weeks=self.interval)
        
        while repetition_num < self.repeat_times:
            if month.includes_date(date1):
                repeats.append(Reminder(date1, text = self.text, original=False,
                                        original_date = original_date, ID = self.ID))
            date1 += td
            if date1 > lastdate:
                break
            repetition_num += 1

        return repeats

class Button:
    def __init__(self, container, text: str, state, 
                 bg = None, fg = None, margin_x = 2, margin_y = 1,
                 pad_x = 8, pad_y = 3,
                 bg_hover = None, fg_hover = None,
                 font_size = None, image = None,
                 height = 1, width = 0,
                 command = None, args = ()):
        self.state = state
        if container == None:
            self.label = tkinter.Label()
            return
        self.text = text
        if not bg_hover:
            self.bg_hover = state.HOVER_BUTTON_BG
        else:
            self.bg_hover = bg_hover
        if not fg_hover:
            self.fg_hover = state.HOVER_BUTTON_FG
        else:
            self.fg_hover = fg_hover
        if not bg:
            self.bg = state.BUTTON_BG
        else:
            self.bg = bg
        if not fg:
            self.fg = state.BUTTON_FG
        else:
            self.fg = fg
        if not font_size:
            self.font_size = state.font_size-4
        else:
            self.font_size = font_size
        self.pad_x = pad_x
        self.pad_y = pad_y
        self.margin_x = margin_x
        self.margin_y = margin_y
        self.label = tkinter.Label(container, font = ("",self.font_size), 
            height = height, width = width, padx = self.pad_x, pady = self.pad_y, 
            borderwidth = 0, relief = "flat", 
            background = self.bg, foreground = self.fg,
            text = text, image = image)
        self.label.bind("<Enter>", lambda e: self.enter())
        self.label.bind("<Leave>", lambda e: self.leave())
        if command:
            self.bind(command, args)

    def enter(self):
        self.label['bg'] = self.bg_hover
        self.label['fg'] = self.fg_hover
    def leave(self):
        self.label['bg'] = self.bg
        self.label['fg'] = self.fg

    def bind(self, command, args=()):
        self.command = command
        self.args = args
        self.label.bind("<Button-1>", lambda e: self.command(*self.args))

    def grid(self, r=None, c=None, sticky="", row=None, column=None):
        if row != None:
            r = row
        if column != None:
            c = column
        self.label.grid(column = c, row = r, padx = self.margin_x, pady = self.margin_y, sticky = sticky)
    def pack(self):
        self.label.pack()
    def destroy(self):
        self.label.destroy()
    def grid_forget(self):
        self.label.grid_forget()

    def set_bg(self, color):
        self.label['bg'] = color
        self.bg = color
    def set_fg(self, color):
        self.label['fg'] = color
        self.fg = color
    def set_hover_bg(self, color):
        self.bg_hover = color
    def set_hover_fg(self, color):
        self.fg_hover = color
    def no_hover_color(self):
        self.label.unbind("<Enter>")
        self.label.unbind("<Leave>")
