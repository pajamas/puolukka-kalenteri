import tkinter
from tkinter import ttk
from calendar import monthrange
from datetime import date
from classes import AddReminderWin, Weekdays, Months, Holidays, Holiday, Month
from windows_reminders import read_reminders, todays_reminders, add_reminder, del_reminder
from loadpaths import puolukka_png, icon_png, arrow_png
from info import info
import tkinter.font as tkFont

class State:
    def __init__(self):
        self.days = []
        self.reminders = read_reminders()
        self.window = tkinter.Tk()
        self.puolukka = tkinter.PhotoImage(file=puolukka_png())
        self.SCREEN_WIDTH = self.window.winfo_screenwidth()
        self.SCREEN_HEIGHT = self.window.winfo_screenheight()
        self.BG_COLOR = "#ffbbcb"
        self.REMINDER_BG = "#f099aa"
        self.REMINDER_HOVER_BG = "#ffa9bc"
        self.FG_BLUE = "#4838b1"
        self.FG_PURPLE = "#823478"
        self.FG_PURPLE_HOVER = "#c54060"
        self.LIGHT_PINK = "#ffcbdb"
        self.LIGHTER_PINK = "#ffcfdf"
        self.LIGHTEST_PINK = "#ffdcea"
        self.BUTTON_BG = "#f3ebb0"
        self.BUTTON_FG = "#af3164"
        self.HOVER_BUTTON_BG = "#f5ea84"
        self.HOVER_BUTTON_FG = "#a32099"
        self.RED = "#FD4E70"
        self.main_frame = tkinter.Frame(self.window,background = self.BG_COLOR)
        self.calendar_grid_frame = tkinter.Frame(self.main_frame)
        self.head_frame = tkinter.Frame(self.main_frame)
        self.font = ""
        self.font_size = 15
        self.month_buttons = []
        self.weekdays = Weekdays()
        self.months = Months()
        self.holidays = Holidays()
        self.month_viewing = Month(date.today().month,date.today().year)
        self.down_arrow = tkinter.PhotoImage(file=arrow_png())
        self.footer = tkinter.Frame(self.main_frame, padx=1,pady=0,
                background = self.BG_COLOR)
        self.menu_button = tkinter.Button()
        self.menu = tkinter.Frame(self.footer,bg=self.BG_COLOR)
        self.plus_button = None
        self.info_button = None
        self.rows = []
        self.x_buttons = []
        self.expanded = False
        self.footer_label = tkinter.Label()
        self.footer_rows_frame = tkinter.Frame(self.footer,bg=self.LIGHT_PINK)
        self.head_label = None

    def start(self):
        self.make_main_win()
        self.gen_month()
        self.window.deiconify()
        self.window.mainloop()

    def what_font(self):
        fonts = [("Noto Serif",15),("Times New Roman",16),
                ("Palatino Linotype",16),("Palatino",16)]
        for font in fonts:
            if font[0] in list(tkFont.families()):
                self.font = font[0]
                self.font_size = font[1]
                break

    def isholiday(self,day,month=0):
        if month == 0:
            month = self.month_viewing.month
        for holiday in self.holidays.list:
            if holiday.date == (day,month):
                return holiday
        return False

    def prev_month(self):
        self.month_viewing -= 1
        self.update_month()

    def next_month(self):
        self.month_viewing += 1
        self.update_month()

    def make_main_win(self):
        self.window.title("Kalenteri")
        self.window.configure(bg=self.BG_COLOR)
        self.window.resizable(False, False)

        self.main_frame.grid(row=0,column=0,sticky="nsew",pady=(1,1))
    
        self.window.bind('<Escape>', lambda e : state.window.destroy())
        
        self.window.bind('<Left>', lambda e : state.prev_month())
        self.window.bind('<Right>', lambda e : state.next_month())

        self.window.bind('<Up>', lambda e : state.shrink())
        self.window.bind('<Down>', lambda e : state.expand())

        self.window.bind('i', lambda e : info(self))
        self.window.bind('a', lambda e : add_reminder(date.today().day-1,self))

        self.window.bind('u', lambda e : self.update_month())
    
    def update_month(self):
        self.head_label['text']  = str(self.month_viewing)
        self.head_label['foreground'] = self.months.colors[self.month_viewing.month-1]

        self.calendar_grid_frame.destroy()

        # make calendar grid
        daysnum = monthrange(*reversed(state.month_viewing))[1]
        self.calendar_grid_frame = tkinter.Frame(self.main_frame, background=self.LIGHT_PINK)
        self.calendar_grid_frame.grid(row=1, sticky="nsew")
        self.days = [tkinter.Button(self.calendar_grid_frame,font=("",self.font_size-4), height=1, width=1, padx=9,
            pady=3, borderwidth=2, relief="ridge", background=self.LIGHT_PINK,
            foreground=state.FG_PURPLE,text = str(i+1),
            activebackground=self.LIGHTEST_PINK, activeforeground=self.FG_PURPLE_HOVER) for i in range(daysnum)]
        
        firstday = date(*reversed(self.month_viewing), 1)
        colstart = firstday.weekday()
    
        # change bg color of days with reminders
        for reminder in state.reminders:
            if state.month_viewing.includes_date(reminder.date):
                self.days[reminder.date.day-1]['background']=self.REMINDER_BG
                self.days[reminder.date.day-1]['activebackground']=self.REMINDER_HOVER_BG
        
        # put days into calendar grid
        for i in range(daysnum):
            self.days[i].grid(row=(int((i+colstart)/7)+1),column=(i+colstart)%7)
            self.days[i]['command'] = lambda arg1=i, arg2=state : add_reminder(arg1,arg2)
            # holiday colors
            for holiday in self.holidays.list:
                if self.month_viewing.month == holiday.date[1] and i+1 == holiday.date[0]:
                    self.days[i]['bg'] = holiday.color
                    self.days[i]['fg'] = "#ffffff"
        rownum = int((daysnum-1+colstart)/7)+1

        # make current day look different in grid
        if (date.today().month,date.today().year) == state.month_viewing():
                self.days[date.today().day-1]['fg']="#8f002c"
                self.days[date.today().day-1]['relief']="raised"

    def gen_month(self):
            # create frame for header
            self.head_frame = tkinter.Frame(self.main_frame, padx=1,pady=4,
                background = self.BG_COLOR, 
                width=self.main_frame.winfo_width())
            self.head_frame.grid(row=0,sticky="we")
            
            # create buttons for navigating months
            self.month_buttons = [tkinter.Button(self.head_frame,text="⮜",bg=self.BUTTON_BG,
                fg=self.BUTTON_FG,width=0,padx=4,pady=1,height=0, 
                command=lambda : self.prev_month(), font=("",10),
                activebackground=self.HOVER_BUTTON_BG,
                activeforeground=self.HOVER_BUTTON_FG
                ),
                tkinter.Button(self.head_frame,text="⮞",width=0,padx=4,pady=1,height=0, 
                command=lambda : self.next_month(), font=("",10),
                bg=state.BUTTON_BG,fg=state.BUTTON_FG,
                activebackground=state.HOVER_BUTTON_BG,
                activeforeground=state.HOVER_BUTTON_FG)]
            self.month_buttons[0].grid(row=0,column=0,sticky="w")
            self.month_buttons[1].grid(row=0,column=10,sticky="e")

            # make label that says name of month + year
            labelFrame = tkinter.Frame(self.head_frame,bg=state.BG_COLOR,width=145,height=26)
            self.head_label = tkinter.Label(labelFrame, 
                text = str(self.month_viewing), 
                font=(self.font, self.font_size-1), background = self.BG_COLOR, 
                foreground = self.months.colors[self.month_viewing.month-1],
                )
            labelFrame.grid(row=0,column=2)
            self.head_label.place(relx=.5, rely=.5, anchor=tkinter.CENTER)
            
            # make icons
            iconFrame = tkinter.Frame(self.head_frame,bg=self.BG_COLOR, height=20,width=23)
            iconFrame2 = tkinter.Frame(self.head_frame,bg=self.BG_COLOR,height=20,width=23)
            icon = tkinter.Label(iconFrame,image=self.puolukka,bg=self.BG_COLOR,padx=5)
            icon2 = tkinter.Label(iconFrame2,image=self.puolukka,bg=self.BG_COLOR,padx=5)
            iconFrame.grid(row=0,column=9,sticky="e")
            iconFrame2.grid(row=0,column=1,sticky="w")
            icon.place(relx=.5, rely=.5, anchor=tkinter.CENTER)
            icon2.place(relx=.5, rely=.5, anchor=tkinter.CENTER)
            
            self.update_month()

            #separator = ttk.Separator(self.main_frame, orient='horizontal')
            #separator.grid(row=2,column=0,sticky="we")

            # create footer
            self.footer = tkinter.Frame(self.main_frame, padx=1,pady=0,
                background = self.BG_COLOR)

            self.footer_label.destroy()
            self.footer_label = tkinter.Label(self.footer,text=f"{self.weekdays.long[date.today().weekday()]} {date.today().strftime('%d.%m.%Y')}",
                    bg=self.BG_COLOR, fg=self.FG_PURPLE,
                    font=(self.font,self.font_size-3))
            
            self.menu_button.destroy()
            self.menu_button = tkinter.Button(self.footer,bg=self.REMINDER_BG,
                    image=self.down_arrow,height=15,width=15, command=self.expand)

            self.footer.grid(row=3,sticky="nsew")
            self.footer.columnconfigure(0,weight=1)
            
            self.footer_label.grid(row=0,column=0,sticky="nsew")
            self.menu_button.grid(row=0,column=1,sticky="e")
    
    def expand(self):
        if self.expanded:
            return
        self.expanded = True
        self.menu_button['command'] = self.shrink

        state.footer_refresh()

    def footer_refresh(self):
        for item in self.footer.winfo_children()[3:]:
            item.destroy()
        
        # create menu
        self.menu = tkinter.Frame(self.footer,bg=self.BG_COLOR)
        self.plus_button = tkinter.Button(self.menu,text="+",bg=self.BUTTON_BG,
                fg=self.BUTTON_FG,
                activebackground=self.HOVER_BUTTON_BG,
                activeforeground=self.HOVER_BUTTON_FG,
                width=0,padx=4,pady=0,height=0)
        self.info_button = tkinter.Button(self.menu,text="i",bg=self.BUTTON_BG,
                fg=self.BUTTON_FG,
                width=1,padx=5,pady=0,height=0)
        self.plus_button['command'] = lambda : add_reminder(date.today().day-1,self)
        self.info_button['command'] = lambda : info(self)
      
        self.footer_rows_frame = tkinter.Frame(self.footer,bg=self.LIGHT_PINK)
        self.footer_rows_frame.columnconfigure(0,weight=1)

        self.rows = []
        self.x_buttons = []
        # list today's reminders
        for reminder in self.reminders:
            if reminder.date == date.today():
                self.rows.append(tkinter.Label(self.footer_rows_frame,text=f"{reminder.text}",
                bg=state.LIGHT_PINK,fg=self.FG_PURPLE,justify="left", wraplength=200))
                self.x_buttons.append(tkinter.Button(self.footer_rows_frame,
                    text="⨯",padx=3,pady=0,
                    bg=state.LIGHT_PINK,fg=self.FG_PURPLE))

        # list holidays
        for holiday in self.holidays.list:
            if holiday.date == (date.today().day,date.today().month):
                self.rows.append(tkinter.Label(self.footer_rows_frame,text=f"{holiday.name}",
                    bg=holiday.color,fg="#ffffff",justify="left",wraplength=200))

        # put menu in grid
        self.menu.grid(column=0,row=50,sticky="nsew")
        self.plus_button.grid(column=0,row=0)
        self.info_button.grid(column=1,row=0)

        # put reminders in grid
        if len(self.rows)>0:
            self.footer_rows_frame.grid(column=0,row=1,sticky="nsew",columnspan=2)
            for i,row in enumerate(self.rows):
                row.grid(row=i,column=0,sticky="nsew")

        # put delete buttons next to reminders
        for i,button in enumerate(self.x_buttons):
            button.grid(row=i,column=1,sticky="e")
            button['command'] = lambda num=i, s=state : del_reminder(num,s)

    def shrink(self):
        if not self.expanded:
            return
        self.expanded = False
        self.menu_button['command'] = self.expand
        for item in self.footer.winfo_children()[2:]:
            item.destroy()

state = State()
state.window.iconphoto(True, tkinter.PhotoImage(file=icon_png()))
state.window.withdraw()
state.what_font()

todays_reminders(state)
