def find(_list, function):
    for i, x in enumerate(_list):
        if function(x):
            return i

    return -1
