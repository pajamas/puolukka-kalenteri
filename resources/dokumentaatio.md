# Dokumentaatio

Puolukkakalenteri on kalenteriohjelma, jolla on graafinen käyttöliittymä. Kuukausia voi selailla yläpalkin nappien avulla tai nuolinäppäimillä. Kalenterissa voi lisätä päiville muistutuksia, joista ohjelma avaamisen yhteydessä muistuttaa kyseisenä päivänä. Kalenteri suljetaan esc-napista.

Ohjelmassa state-luokka sisältää tärkeitä muuttujia ja välittää tietoa funktioiden välillä.

Kalenteri avataan Linuxilla suorittamalla `kalenteri.py`-tiedosto, ja Windowsilla `Kalenteri_Windows.pyw`-tiedosto.

## Ulkopuoliset kirjastot

`tkinter`

* käytetään graafisen käyttöliittymän luomiseen


`datetime`

* käytetään päivämäärien käsittelyyn


`calendar`

* tästä kirjastosta käytetään vain `monthrange()`-metodia, joka kertoo tietyssä kuukaudessa olevien päivien määrän


`sys`

* tästä kirjastosta käytetään `stderr.write()`-metodia, joka kirjoittaa stderriin virheviestin, jos `Reminders.txt`-tiedostoa ei löydy (Linux-versiossa)


## Luokat


`State()`

* sisältää ohjelman globaalit muuttujat ja pääikkunan
* välittää tietoa funktioiden välillä


`AddReminderWin()`

* sisältää 'Lisää muistutus' -ikkunan osat
* muistutusikkunan luominen luo uuden instanssin tästä luokasta


`Weekdays()`

* sisältää listan viikonpäivien suomenkielisistä nimistä ja toisen listan niiden lyhenteistä


`Months()`

* sisältää listan kuukausien suomenkielisistä nimistä ja toisen niiden väreistä (käytetään kalenterin pääikkunan yläpalkissa fontin värinä)


`Holiday(date, name, color)`

* käytetään juhlapäivien määrittelyyn
* ottaa argumentiksi päivämäärän, nimen ja värin, jota käytetään esim. kalenteriruudun taustavärinä


`Holidays()`

* käytetään juhlapäivien varastoimiseen


`Month(month,year)`

* kuvaa kalenterikuukautta, sisältää kuukauden ja vuoden
* pääinstanssi on state.month\_viewing, joka kuvaa kuukautta joka on auki kalenterissa
* käytetään esim. kuukausien selailuun


`Reminder(date,text)`

* muistutus
* ottaa argumentiksi päivämäärän (date-objekti) ja tekstin


## Funktiot

### `kalenteri.py`

`state.start()`

* kutsuu funktioita, jotka luovat pääikkunan


`state.what_font()`

* valitsee ohjelmalle fontin


`state.isholiday(day,month=0)`

* ottaa argumentiksi päivän ja kuukauden, jos kuukautta ei anneta niin oletusarvo on kuukausi mikä on auki kalenterissa
* jos kyseinen päivä on juhlapäivä, palauttaa juhlan Holiday() objektin. Muuten palauttaa False


`state.prev_month()` ja `state.next_month()`

* käytetään kuukausien selailuun
* kutsuvat `state.gen_month()`-funktion, joka vaihtaa kalenterin sivua


`state.make_main_win()`

* kutsutaan ohjelman alussa
* luo ohjelman ikkunan
* bindaa esc-napin sulkemaan ikkunan ja oikean ja vasemman nuolinäppäimen selaamaan kuukausia


`state.gen_month()`

* näyttää uuden sivun kalenterista
* kutsutaan ohjelman alussa ja silloin kun kalenterin sivua vaihdetaan


`state.footer_refresh()`

* näyttää widgetit ohjelman alaosassa, jossa on tämänpäiväiset muistutukset, infonappi ja muistutusten lisäysnappi


`state.expand()`

* vaihtaa alaosan napin kutsumaan `state.shrink()`-funktiota
* näyttää ohjelman alaosan (kutsuu `footer_refresh`-funktion)


`state.shrink()`

* vaihtaa alaosan napin kutsumaan `state.expand()`-funktiota
* tuhoaa ohjelman alaosan widgetit


### `classes.py`

`AddReminderWin.close()`

* tuhoaa ikkunan ja poistaa instanssin


`Month.includes_date(date)`

* ottaa argumentiksi päivämäärän (date-objektin)
* kertoo, sisältyykö päivämäärä kyseiseen kuukauteen
* palauttaa True tai False


### `reminders.py`

`add_reminder(day,state)`

* luo uuden AddReminderWin-instanssin
* luo ikkunan ja sen sisällön:  
    * otsikko, jossa päivämäärä
    * tekstikenttä, johon kirjoitetaan muistutusteksti
    * nappi, josta lisätään muistutus
    * lista päivän muistutuksista ja napit niiden poistamiseen


`write_reminder(state,add_reminder_win)`

* ottaa argumentiksi staten ja instanssin AddReminderWin-luokasta
* hakee tekstikentästä lisättävän muistutuksen tekstin
* lisää muistutuksen state-luokan muistutuslistaan
* kirjoittaa muistutuksen `Reminders.txt`-tiedostoon


`del_reminder(i,state,add_reminder_win)`

* ottaa argumentiksi staten, instanssin AddReminderWin-luokasta ja numeron i, joka kertoo kuinkaa mones muistutus AddReminderWin-ikkunasta täytyy poistaa
* poistaa muistutuksen ikkunasta
* kirjoittaa `Reminders.txt`-tiedoston uudelleen ilman poistettua muistutusta
* poistaa muistutuksen `state.reminders`-listasta


`todays_reminders(state)`

* tarkistaa, onko tälle päivälle muistutuksia
    * jos ei, kutsuu `state.start()`-funktion ja ei tee muuta
* jos on, luo ikkunan niiden näyttämiseen


`close_todays_reminders(state,today_reminder_win)`

* sulkee muistutusikkunan
* kutsuu `state.start()`-funktion


`read_reminders()`

* lukee `Reminders.txt`-tiedostosta muistutukset
* tallentaa muistutukset `state.reminders`-listaan
* Linux-versio: jos `Reminders.txt`-tiedostoa ei löydy, kirjoittaa stderriin virheviestin


### `loadpaths.py`

Tässä tiedostossa on funktioita, jotka palauttavat polun eri resursseihin (kuviin ja `Reminders.txt`-tiedostoon). Ohjelman asennukseen Linuxilla käytettävä `install.sh`-skripti muokkaa näitä polkuja.


### `info.py`

`info(state)`

* luo infoikkunan, joka avataan pääikkunan alaosassa olevasta i-napista
