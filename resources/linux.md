# Asennus Linuxilla

Avaa terminaali (esim. Konsole) ja suorita nämä komennot (kirjoita komento ja paina enter):

`mkdir git`

`cd git`

`git clone https://gitlab.com/pajamas/puolukka-kalenteri.git`

`cd puolukka-kalenteri`

Jos haluat löytää kalenterin hausta: (muuten avaa kalenteri komennolla `./kalenteri.py`)

`./install.sh`

Huom. jos haluat terminaalissa liittää komennon, se tehdään yleensä painamalla `ctrl + shift + v`
